# Polynomial Operations Console Application

This console application, written in C++, allows you to perform various operations on polynomials. Developed by Yassir Abbassi, this program enables users to create, manipulate, and analyze polynomial expressions interactively through a simple menu-driven interface.

## Project Overview

### Project Description

This C++ program is designed for working with polynomials. It offers a menu-driven interface that allows you to:

1. **Create Polynomials**: Define polynomials by specifying their degrees and coefficients.

2. **Show Polynomials**: Display the created polynomials.

3. **Compute Polynomial Value**: Evaluate a polynomial at a specific value of `x`.

4. **Polynomials Addition**: Perform addition of two polynomials.

5. **Sort Polynomial**: Sort the monomials within a polynomial.

6. **Delete Polynomial**: Remove a polynomial from the collection.

### Menu of Options

Here is a menu of options that you can choose from:

```
Menu of options:
1. Create Polynomials       2. Show Polynomials
3. Compute Polynomial Value 4. Polynomials Addition
5. Sort Polynomial          6. Delete Polynomial
0. Show Menu                9. Exit
```

The program guides you through each operation and provides feedback to help you manage and manipulate polynomials efficiently.

### Command Line Interface

The application interacts with the user through the command line interface. You can select various options by entering the corresponding command numbers. The program also includes error handling to ensure a smooth user experience.

### Dynamic Polynomial Management

The program allows dynamic management of polynomial expressions, enabling users to create, modify, and work with multiple polynomials.

## Project Goals

- Create and manage polynomial expressions dynamically.
- Perform mathematical operations on polynomials.
- Provide a user-friendly menu-driven interface.
- Enable users to evaluate polynomial expressions at specific values.
- Offer a variety of polynomial-related operations.
