#include "Polynomial.h"

#include <string>

using namespace std;


int main()
{
	int option;
	unsigned nbre_of_poly=0;
	Polynomial *P = new Polynomial[0];
	
	cout << "\nMenu of options:\n";
	cout << "\t1. Create Polynomials" << "\t\t\t2. Show Polynomials" << endl;
	cout << "\t3. Compute Polynomial Value" << "\t\t4. Polynomials Addition" <<endl;
	cout << "\t5. Sort Polynomial" <<"\t\t\t6. Delete Polynomial" <<endl;
	cout << "\n\t0. Show Menu" << "\t\t\t\t9. Exit\n";	
	
	do{
		cout << "\nEnter command number: ";
		cin >> option;
		
		do{
			if(option<0 || option>9){
				printf("\aErreur[0]: Check the menu for valid command number.\n");
				cin >> option;
			}
		}while(option<0 || option>9);
		
		if(option==9)
		{
			string ch;
			cout << "Confirm exit: (Y/N) ";
			cin >> ch;
			if(ch == "Y" || ch == "y")
			{
				break;
			}else{
				continue;
			}	
		}
		
		if(option==0)
		{
			cout << "\nMenu of options:\n";
			cout << "\t1. Create Polynomials" << "\t\t\t2. Show Polynomials" << endl;
			cout << "\t3. Compute Polynomial Value" << "\t\t4. Polynomials Addition" <<endl;
			cout << "\t5. Sort Polynomial" <<"\t\t\t6. Delete Polynomial" <<endl;
			cout << "\n\t0. Show Menu" << "\t\t\t\t9. Exit\n";
		}
		
		if(option==1)
		{
			cout << "\nEnter number of polynomials to set: ";
			unsigned n = 0;
			cin >> n;
			
			Polynomial* A = new Polynomial[nbre_of_poly+n];
			std::copy(P, P + std::min(nbre_of_poly, nbre_of_poly+n), A);
			delete[] P;
			P = A;
						
			for(int i = nbre_of_poly ; i<nbre_of_poly+n ; i++)
			{
				cout << "\tPolynomial " << i+1 <<":\n";
				
				cout << "\tEnter degree: ";
				float d;
				cin >> d;
				
				cout << "\tEnter size of Polynomial: ";
				unsigned s;
				cin >> s;
				
				Monomial m[s];
				cout << endl;
				for(int j=0; j<s; j++)
				{
					cout << "\t\tMonomial " << j+1 <<"\n";
					
					unsigned u;
					float f;
					
					cout << "\t\tDegree: ";
					cin >> u;
					m[j].setDeg(u);
					
					cout << "\t\tCoeff: ";
					cin >> f;
					m[j].setCoeff(f);
					
					cout << endl;
				}
				Polynomial tmp(d, m, s);
				P[i] = tmp;
				cout << "\n";
			}
			nbre_of_poly += n;
		}
                    
		if(option==2)
		{
			cout << endl;
			for(int i=0; i<nbre_of_poly ; i++)
			{
				cout << "Polynomial " << i+1 <<endl;
				P[i].displayPolynomial();
				cout << "\n";
			}
		}

		if(option==3)
		{
			cout << "\nSelect Number of Polynomial: ";
			unsigned nbre;
			cin >> nbre;
			
			cout << "\nEnter X: ";
			float x;
			cin >> x;
			
			P[nbre-1].displayPolynomial();
			cout << "\nP[" << x << "] = " << P[nbre-1].PolyValue(x) << endl;
		}
		
		if(option==4)
		{
			cout << "\nSelect Number of 1st Polynomial: ";
			unsigned n1;
			cin >> n1;

			cout << "\nSelect Number of 2nd Polynomial: ";
			unsigned n2;
			cin >> n2;
			
			Polynomial P3 = P[n1-1] + P[n2-1];
			
			Polynomial* A = new Polynomial[nbre_of_poly+1];
			std::copy(P, P + std::min(nbre_of_poly, nbre_of_poly+1), A);
			delete[] P;
			P = A;
			
			nbre_of_poly++;
			P[nbre_of_poly-1] = P3;
			cout << endl;
		}
		
		if(option==5)
		{
			cout << "\nSelect Number of Polynomial: ";
			unsigned nbre;
			cin >> nbre;
			
			P[nbre-1] = P[nbre-1].sortMonomials();
			cout << endl;
		}
		
		if(option==6)
		{
			cout << "\nSelect Number of Polynomial: ";
			unsigned nbre;
			cin >> nbre;
			
			for(int i = nbre ; i<nbre_of_poly; i++)
			{
				P[i-1] = P[i];
			}
			nbre_of_poly--;
			cout << endl;
		}
	}
	while(option>-1 && option<10);
}
