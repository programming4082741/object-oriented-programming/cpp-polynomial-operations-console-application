#include "Monomial.h"

// Class definition
// Polynome Class
class Polynomial
{
	// Attributes
	private:
		float degree;
		Monomial mono[20];
		unsigned size;
	
	protected:
		Polynomial* address(void)
		{
			return this;
		}
	
	public:
		// Constructors
		Polynomial(float d, Monomial m[], unsigned s)
		{
			this->degree=d;
			this->size=0;
			
			Monomial mtmp;
			
			unsigned indx=0;
			for(int i=0; i<s ; i++)
			{
				unsigned br = 1;
				mtmp = m[i];
				for(int j=0; j<i ; j++)
				{
					if(mono[j].getDeg() == mtmp.getDeg()){
						this->mono[j].setCoeff( this->mono[j].getCoeff()+mtmp.getCoeff());
						br = 0;
						break;
					}
				}
				if(br != 0)
				{
					this->mono[indx] = m[i];
					this->size++;
					indx++;
				}
			}
			float k = this->mono[0].getDeg();
			for(int i=1; i<s; i++)
			{
				if(k < this->mono[i].getDeg())
				{
					k = this->mono[i].getDeg();
				}
			}
			this->degree = k;
		}
		
		// Default Constructor
		Polynomial()
		{
			this->degree = 0;
			this->size = 0;
		}
		
		float PolyValue(float x)
		{
			float v=0;
			for(int i=0 ; i<this->size ; i++)
			{
				v += this->mono[i].monoValue(x);
			}
			return v;
		}
		
		// Deletion
		void deleteMonomial(unsigned indx)
		{
			this->mono[indx].setCoeff(0);
			this->mono[indx].setDeg(0);
			
			for(int i=indx+1; i<20 ; i++)
			{
				this->mono[i-1] = mono[i];
			}
		}
		
		void deleteAllMonomials()
		{
			delete [] this->mono;
			this->degree = 0;
			this->size = 0;
		}
		
		
		// Setters
		void setDegree(float d)
		{
			if(this->degree < d)
			{
				Monomial m(1,d);
				
				this->mono[this->size] = m;
				this->size++;
				this->degree = d;
			}
		}
		
		void setMonomial(Monomial m)
		{
			unsigned check = 1;
			for(int i=0 ; i<this->size; i++)
			{
				if(this->mono[i].getDeg() == m.getDeg())
				{
					this->mono[i] = m;
					check = 0;
				}
			}
			if( check != 0)
			{
				this->mono[this->size]=m;
				this->size++;
			}
		}
		
		
		// Getters
		float getDegree()
		{
			return this->degree;
		}
		
		Monomial getMonomial(string option, float value)
		{
			if(option == "degree")
			{
				for(int i=0; i<this->size; i++)
				{
					if(this->mono[i].getDeg()==value)
					{
						return this->mono[i];
					}
				}
			}
			else if(option == "index")
			{
				return this->mono[(int)value];
			}
		}
		
		
		// Utilities
		Polynomial sortMonomials()
		{
			Polynomial sorted(this->degree, this->mono, this->size);
			Monomial m;
			
			for( int i=0 ; i < this->size ; i++ )
			{
				for( int j=0 ; j < this->size-i ; j++ )
				if( sorted.mono[j].getDeg() < sorted.mono[j+1].getDeg() )
				{
					m = sorted.mono[j];
					
					sorted.mono[j] = sorted.mono[j+1];
					sorted.mono[j+1] = m;
				}
			}
			return sorted;
		}
		
		void displayPolynomial()
		{
			cout << "Degree: " << this->degree << "\t Size: " << this->size << "\nP[X] = ";
			if(this->mono[0].getCoeff()!=0)
			{
				cout <<  mono[0].getCoeff() << "x^" << this->mono[0].getDeg();
			}
			for(int i=1 ; i < this->size ; i++)
			{
				if(this->mono[i].getCoeff() == 0)
				{
					//continue;
				}
				cout << " + " << this->mono[i].getCoeff() << "x^" << this->mono[i].getDeg();
			}
			cout << endl;
		}
		
		// Overloading Operator
		Polynomial operator+(Polynomial const &p)
		{				
			float d = this->degree;
			if(d < p.degree)
			{
				d = p.degree;
			}
			
			unsigned s1 = this->size;
			unsigned s2 = p.size;
			unsigned s = s1+s2;
			Monomial m[s];
			
			for(int i = 0 ; i < s1; i++)
			{
				m[i] = this->mono[i];
			}
			for(int i = 0 ; i < s2; i++)
			{
				m[i+s1] = p.mono[i];
			}
			
			Polynomial tmp(d, m, s);
			return tmp;
		}
		/*
		// Deconstructor
		~Polynomial()
		{
			cout << "END " <<this->address() <<endl;
		}*/
};