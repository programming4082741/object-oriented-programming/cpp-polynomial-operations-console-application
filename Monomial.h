#include <iostream>
#include <cmath>

using namespace std;


// Class definition
// Monome Class
class Monomial
{
	// Attribute
	private:
		float coeff;
		unsigned deg;
	
	protected:
		// adress retriever
		Monomial* address(void)
		{
			return this;
		}
	
	public:
		// Constructors
		Monomial(float coeff, unsigned deg)
		{
			this->coeff = coeff;
			this->deg = deg;
		}

		Monomial()
		{
			this->coeff = 0;
			this->deg = 0;
		}
		
		float monoValue(float x)
		{
			return this->coeff*pow(x, this->deg);
		}
		
		// Overloading Operator
		Monomial operator+(Monomial const &m)
		{
			Monomial tmp;
			if(this->getDeg() == m.deg)
			{
				tmp.setCoeff(this->getCoeff() + m.coeff);
				tmp.setDeg(this->getDeg());
			}
			return tmp;
		}
		
		Monomial operator-(Monomial const &m)
		{
			Monomial tmp;
			if(this->getDeg() == m.deg)
			{
				tmp.setCoeff(this->getCoeff() - m.coeff);
				tmp.setDeg(this->getDeg());
			}
			return tmp;
		}
		
		Monomial operator*(Monomial const &m)
		{
			Monomial tmp;
			tmp.setCoeff(this->getCoeff() * m.coeff);
			tmp.setDeg(this->getDeg()+m.deg);
			return tmp;
		}

				
		// Getters & Setters
		float getCoeff()
		{
			return this->coeff;
		}
		void setCoeff(float c)
		{
			this->coeff = c;
		}
		
		unsigned getDeg()
		{
			return this->deg;
		}
		void setDeg(unsigned d)
		{
			this->deg = d;
		}
		
		// Display
		void displayMono()
		{
			if(this->coeff != 0)
			{
				cout << this->coeff;
				
				if(this->deg == 1) {
					cout << "x";
				}
				if(this->deg > 1){
					cout << "x^"<< this->deg << endl;
				}
			}
		}
		
		/*
		// Destructor
		~Monomial()
		{
			cout << "END " <<this->address() <<endl;
		}*/

};